import datetime

import flask
import icalendar
import requests
import requests_ntlm

app = flask.Flask(__name__)
app.config.from_envvar('RMS_SETTINGS')


def strptime(s):
    if s.endswith('Z'):
        dt = datetime.datetime.strptime(s, '%Y-%m-%dT%H:%M:%SZ')
        return dt.astimezone(datetime.timezone.utc)
    else:
        return datetime.datetime.fromisoformat(s)


def get_filters(**kwargs):
    filters = []

    filters.append("startswith(Status, '03 -')")  # booked

    if 'after' in kwargs:
        strptime(kwargs['after'])
        filters.append("Bis ge '{}'".format(kwargs['after']))
    if 'before' in kwargs:
        strptime(kwargs['before'])
        filters.append("Von le '{}'".format(kwargs['before']))

    if 'room' in kwargs:
        room = int(kwargs['room'], 10)
        if room not in app.config['RMS_ALLOWED_ROOMS']:
            raise ValueError
        rooms = [room]
    else:
        rooms = app.config['RMS_ALLOWED_ROOMS']
    filters.append('({})'.format(' or '.join(
        'Raum/Id eq {}'.format(i) for i in rooms
    )))

    return ' and '.join(filters)


def get_from_sharepoint(base_url, username, password, filters):
    # http://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part1-protocol.html
    return requests.get(
        base_url + "_api/web/lists/getbytitle('Reservations')/items",
        params={
            '$select': 'Title,Von,Bis,GanztaegigesEreignis',
            '$filter': filters,
        },
        headers={
            'Accept': 'application/json',
        },
        auth=requests_ntlm.HttpNtlmAuth(username, password),
    )


def convert_sharepoint_to_ics(data):
    cal = icalendar.Calendar()
    cal.add('prodid', '-//MPIB//RMS Bridge//')
    cal.add('version', '2.0')

    for item in data['value']:
        event = icalendar.Event()
        event.add('summary', item['Title'])
        if item['GanztaegigesEreignis']:
            event.add('dtstart', strptime(item['Von']).date())
            event.add('dtend', strptime(item['Bis']).date())
        else:
            event.add('dtstart', strptime(item['Von']))
            event.add('dtend', strptime(item['Bis']))
        cal.add_component(event)

    return cal.to_ical()


@app.route('/')
def get():
    try:
        filters = get_filters(**flask.request.args)
    except ValueError:
        return '', 400

    req = get_from_sharepoint(
        app.config['RMS_URL'],
        app.config['RMS_USER'],
        app.config['RMS_PASSWORD'],
        filters,
    )
    if req.status_code == 400:
        return '', 400
    elif not req.ok:
        req.raise_for_status()

    ics = convert_sharepoint_to_ics(req.json())
    return ics, {'Content-Type': 'text/calendar'}

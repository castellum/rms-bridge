# RMS Bridge

This is a simple service that converts events from the sharepoint-based room
booking system MAX RMS to ICS.

## Quickstart

```
$ python3 -m venv .venv
$ . .venv/bin/activate
$ pip install -r requirements.txt
$ FLASK_APP=rms_bridge FLASK_DEBUG=1 RMS_SETTINGS=example.cfg flask run
$ curl http://localhost:5000/
```

## Parameters

-   `after` (datetime): only include events after this
-   `before` (datetime): only include events before this
-   `room` (integer): only include events belonging to this room ID

## Security

RMS Bridge does not provide authentication. It is recommended that you restrict
`RMS_ALLOWED_ROOMS` to a set of public rooms.

The minimize the potential for unintended data leaks, RMS Bridge only forwards
the following fields: `Title`, `Von`, `Bis`

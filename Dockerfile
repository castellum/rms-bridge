FROM buildpack-deps:stable

ENV PKGS python3 python3-pip python3-wheel uwsgi uwsgi-plugin-python3

RUN useradd uwsgi
RUN apt-get update -q && apt-get install -y --no-install-recommends $PKGS && rm -rf /var/lib/apt/lists/*

WORKDIR code/
COPY rms_bridge.py requirements.txt uwsgi.ini LICENSE ./
RUN pip3 install -r requirements.txt

CMD uwsgi uwsgi.ini --die-on-term
